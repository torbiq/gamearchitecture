﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryPoint : MonoBehaviour {
    private void Start()
    {
        IService[] servicesToInitialize = new IService[]
        {
            MonoListener.Instance as IService,
            PauseController.Instance as IService,
            KeyListener.Instance as IService,
            LocalizationManager.Instance as IService,
            ResourceManager.Instance as IService,
            AudioController.Instance as IService,
            PopupManager.Instance as IService,
            SceneManager.Instance as IService,
            BackController.Instance as IService,
            SceneLoadingUIController.Instance as IService,
            GameSettingsController.Instance as IService,
            AgeController.Instance as IService,
            AdsController.Instance as IService,
        };
        int length = servicesToInitialize.Length;

        for (int i = 0; i < length; i++)
        {
            servicesToInitialize[i].Init();
        }
        for (int i = 0; i < length; i++)
        {
            servicesToInitialize[i].Subscribe();
        }
        for (int i = 0; i < length; i++)
        {
            servicesToInitialize[i].Load();
        }
        SceneManager.Instance.ChangeScene(SceneEnum.MainMenu);
    }
}
