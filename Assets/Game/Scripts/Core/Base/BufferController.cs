﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;

public enum BufferKey
{

}

public static class BufferController {
    static BufferController()
    {
        _mainInfo = new Dictionary<BufferKey, object>();
    }
    private static Dictionary<BufferKey, object> _mainInfo;
    public static object GetData(BufferKey key) {
        object returnedInfo = null;
        _mainInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
        }
        return returnedInfo;
    }
    public static T GetData<T>(BufferKey key) {
        object returnedInfo = null;
        _mainInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
            return default(T);
        }
        return (T)returnedInfo;
    }
    public static void SetData(BufferKey key, object value) {
        if (_mainInfo.ContainsKey(key)) {
            _mainInfo[key] = value;
            Log.Warning("Key [" + key + "] info will be replaced in main data of BufferController");
            return;
        }
        _mainInfo.Add(key, value);
    }
    public static bool RemoveData(BufferKey key) {
        return _mainInfo.Remove(key);
    }
}