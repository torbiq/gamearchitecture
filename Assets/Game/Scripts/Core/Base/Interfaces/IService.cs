﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IService
{
    /// <summary>
    /// Loading of private members.
    /// </summary>
    void Init();
    /// <summary>
    /// Subscribing between services.
    /// </summary>
    void Subscribe();
    /// <summary>
    /// Initial usages of services each one by other.
    /// </summary>
    void Load();
}