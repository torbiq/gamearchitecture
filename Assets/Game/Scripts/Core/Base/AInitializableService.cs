﻿using Game.Extensions;
using UnityEngine;

public abstract class ASingletoneService<Tservice, Tbase> : MonoBehaviour, IService
    where Tservice : ASingletoneService<Tservice, Tbase>, Tbase
    where Tbase : class
{
    public static Tbase Instance { get; private set; }

    #region Unity
    private void Awake()
    {
        if (Instance != null)
        {
            Debug.LogError("Initializable service " + typeof(Tservice).ToString() + " already instanced. Destroying second object.");
            Destroy(gameObject);
            return;
        }
        Instance = (Tservice)this;
        DontDestroyOnLoad(transform.GetBaseParent().gameObject);
    }
    private void OnDestroy()
    {
        if (this == (Tservice)Instance)
        {
            Instance = null;
        }
    }
    #endregion

    #region Service
    public virtual void Init()
    {

    }
    public virtual void Subscribe()
    {

    }
    public virtual void Load()
    {

    }
    #endregion
}
