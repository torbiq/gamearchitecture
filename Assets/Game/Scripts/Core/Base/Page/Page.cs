﻿using UnityEngine;

public enum PageType
{
    Main,
}

public abstract class Page : MonoBehaviour
{
    public PageType Type { get; protected set; }
    public float ShowDuration { get; protected set; }
    public float HideDuration { get; protected set; }
    public bool IsShown { get; protected set; }
    public bool IsInTransition { get; protected set; }
    public bool CanStayWhenSwitchingScene { get; protected set; }
    public bool CanShowDuringTransition { get; protected set; }

    public virtual void Init()
    {
    }
    public virtual bool Show()
    {
        return false;
    }
    public virtual bool Hide()
    {
        return false;
    }
}
