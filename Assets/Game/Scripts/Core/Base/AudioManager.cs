﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using Game.Extensions;


public class AudioManager : MonoBehaviour {
    private readonly List<SceneEnum> _overrideOwnControl = new List<SceneEnum>
    {
    };
    private readonly Dictionary<SceneEnum, string> _overridedMusic = new Dictionary<SceneEnum, string> {
    };
    private bool musicControlEnabled
    {
        get
        {
            SceneEnum currentScene = SceneManager.GetCurrentSceneFromUnity();
            return !(_overrideOwnControl.Contains(currentScene) || _overridedMusic.ContainsKey(currentScene));
        }
    }
    private List<AudioClip> _allSongs;
    public List<AudioClip> allSongs {
        get { return _allSongs; }
    }
    private List<AudioClip> _activeClips;
    public List<AudioClip> activeClips {
        get { return _activeClips; }
    }
    private List<AudioClip> _paidClips;
    public List<AudioClip> paidClips {
        get { return _paidClips; }
    }

    private Tween _musicPlayTween;
    private float _musicSwitchDelay = 30;
    public float MusicSwitchDelay {
        get {
            return _musicSwitchDelay;
        }
        set {
            _musicSwitchDelay = value;
            if (OnMusicSwitchDelayChanged != null) {
                OnMusicSwitchDelayChanged(_musicSwitchDelay);
            }
        }
    }
    private event System.Action<float> OnMusicSwitchDelayChanged;
    private readonly int _defaultAudioIndexToPlay = 0;
    private int _currentActivePlayingIndex = 0;
    public int currentActivePlayingIndex
    {
        get { return _currentActivePlayingIndex; }
    }
    public event System.Action<string> OnMusicChangedEvent;

    private void Awake()
    {
        if (_instance) {
            if (_instance != this) {
                DestroyImmediate(gameObject);
                return;
            }
        }
        DontDestroyOnLoad(gameObject);
    }
    private void OnActiveLevelChanged(SceneEnum scene)
    {
        if (_overridedMusic.ContainsKey(scene))
        {
            AudioController.Instance.PlayMusic(_overridedMusic[scene], 1f);
        }
        else
        {
            if (_activeClips.Count == 0)
            {
                AudioController.Instance.StopMusic(1f);
            }
            else
            {
                if (AudioController.Instance.IsMusicPlaying())
                {
                    if (!_activeClips.Contains(_allSongs[_currentActivePlayingIndex]))
                    {
                        PlayNextAvailableSong();
                    }
                }
                else
                {
                    PlayChosenSong(_currentActivePlayingIndex);
                }
            }
        }
    }
    public void PlayChosenSong(int chosenIndex)
    {
        if (chosenIndex >= _allSongs.Count)
        {
            Debug.LogError("Out of songs range");
            return;
        }
        if (chosenIndex == _currentActivePlayingIndex && AudioController.Instance.IsMusicPlaying())
        {
            return;
        }
        if (_musicPlayTween != null)
        {
            if (_musicPlayTween.IsPlaying())
            {
                _musicPlayTween.Kill();
            }
        }
        if (_overrideOwnControl.Contains(SceneManager.GetCurrentSceneFromUnity()))
        {
            PlayGivenClipIndex(chosenIndex);
        }
        else {
            Log.Error("Scene doesn't have rights to change music, add it to overrided list.");
        }
        _musicPlayTween = DOVirtual.DelayedCall(_musicSwitchDelay, PlayNextAvailableSong);
    }
    public void PlayAvailableMusic()
    {
        if (!AudioController.Instance.IsMusicPlaying())
        {
            if (IsActiveClip(_allSongs[_currentActivePlayingIndex]))
            {
                PlayGivenClipIndex(_currentActivePlayingIndex);
            }
            else
            {
                PlayNextAvailableSong();
            }
        }
    }
    public List<AudioClip> GetAllMusicList()
    {
        List<AudioClip> musicList = new List<AudioClip>();
        musicList.AddRange(_allSongs);
        return musicList;
    }
    public bool IsActiveClip(AudioClip audioClip)
    {
        return _activeClips.Contains(audioClip);
    }
    public bool IsActiveClip(string audioClipName)
    {
        for (int i = 0; i < _activeClips.Count; i++)
        {
            if (_activeClips[i].name == audioClipName)
            {
                return true;
            }
        }
        return false;
    }
    public bool ActivateClip(AudioClip audioClip)
    {
        if (!IsClipPurchased(audioClip))
        {
            //BillingManager.Instance.Purchase(BillingManager.SKU_ADMOB);
            return false;
        }
        if (_allSongs.Contains(audioClip))
        {
            _activeClips.Add(audioClip);
            return true;
        }
        return false;
    }
    public bool ActivateClip(string audioClipName)
    {
        if (!IsClipPurchased(audioClipName))
        {
            //BillingManager.Instance.Purchase(BillingManager.SKU_ADMOB);
            return false;
        }
        int songsCount = _allSongs.Count;
        for (int i = 0; i < songsCount; i++)
        {
            if (_allSongs[i].name == audioClipName)
            {
                _activeClips.Add(_allSongs[i]);
                return true;
            }
        }
        return false;
    }
    public bool DeactivateClip(AudioClip audioClip)
    {
        return _activeClips.Remove(audioClip);
    }
    public bool DeactivateClip(string audioClipName)
    {
        for (int i = 0; i < _activeClips.Count; i++)
        {
            if (_activeClips[i].name == audioClipName)
            {
                _activeClips.RemoveAt(i);
                return true;
            }
        }
        return false;
    }
    public bool IsClipPurchased(string audioClipName)
    {
        //if (PrototypeHelpers.IsAdsEnabled())
        //{
        //    int paidCount = _paidClips.Count;
        //    for (int i = 0; i < paidCount; i++)
        //    {
        //        if (_paidClips[i].name == audioClipName)
        //        {
        //            return false;
        //        }
        //    }
        //}
        return true;
    }
    public bool IsClipPurchased(AudioClip audioClip)
    {
        //if (PrototypeHelpers.IsAdsEnabled())
        //{
        //    return !_paidClips.Contains(audioClip);
        //}
        return true;
    }
    private void PlayNextAvailableSong()
    {
        if (_musicPlayTween != null)
        {
            if (_musicPlayTween.IsPlaying())
            {
                _musicPlayTween.Kill();
            }
        }

        _musicPlayTween = DOVirtual.DelayedCall(_musicSwitchDelay, PlayNextAvailableSong);

        if (!musicControlEnabled)
        {
            return;
        }

        int? closestAvailable = GetClosestAvailableSong();

        if (closestAvailable != null)
        {
            PlayGivenClipIndex(closestAvailable.Value);
        }

        if (OnMusicChangedEvent != null)
        {
            OnMusicChangedEvent(_allSongs[_currentActivePlayingIndex].name);
        }
    }
    private void PlayGivenClipIndex(int chosenIndex)
    {
        _currentActivePlayingIndex = chosenIndex;
        AudioController.Instance.PlayMusic(_allSongs[_currentActivePlayingIndex], 1);

        if (OnMusicChangedEvent != null) {
            OnMusicChangedEvent(_allSongs[_currentActivePlayingIndex].name);
        }
    }
    private int? GetClosestAvailableSong()
    {
        int i = _currentActivePlayingIndex + 1;
        i %= _allSongs.Count;

        for (int counter = 0; counter < _allSongs.Count; ++i, ++counter, i %= _allSongs.Count)
        {
            var song = _allSongs[i];
            if (_activeClips.Contains(song) && !_paidClips.Contains(song))
            {
                return i;
            }
        }
        return null;
    }
    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = new GameObject().AddComponent<AudioManager>();
                _instance.name = "AudioManager";

                _instance._allSongs = new List<AudioClip>();
                _instance._activeClips = new List<AudioClip>();
                _instance._paidClips = new List<AudioClip>();
                UnityEngine.SceneManagement.SceneManager.activeSceneChanged += delegate { _instance.OnActiveLevelChanged(SceneManager.GetCurrentSceneFromUnity()); };

                for (int i = 0; i < _instance.allSongs.Count; i++) {
                    _instance._activeClips.Add(_instance._allSongs[i]);
                }
                //for (int i = 8; i < _instance._allSongs.Count; i++) {
                //    _instance._paidClips.Add(_instance._allSongs[i]);
                //}
                //if (PrototypeHelpers.AdsEnabled()) {
                //    for (int i = 8; i < _instance._allSongs.Count; i++) {
                //        _instance._activeClips.Remove(_instance._allSongs[i]);
                //    }
                //}

                _instance._currentActivePlayingIndex = _instance._defaultAudioIndexToPlay;

            }
            return _instance;
        }
    }
}
