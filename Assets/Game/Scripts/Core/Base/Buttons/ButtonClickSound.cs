﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonClickSound : MonoBehaviour
{
    [SerializeField] private AudioClip audioClip = null;
    [SerializeField] private string audioClipName = "";
    [SerializeField] private float delayFade = 0.0f;
    private Button _button;
    private void Awake()
    {
        (_button = GetComponent<Button>()).onClick.AddListener(OnButtonClickedHandler);
    }
    private void OnButtonClickedHandler()
    {
        if (audioClip)
            AudioController.Instance.Play(audioClip, delayFade);
        else if (audioClipName != "")
            AudioController.Instance.Play(audioClipName, delayFade);
    }
}
