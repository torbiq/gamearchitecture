﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LocalizationButton : MonoBehaviour
{
    [SerializeField] private Localization _targetLocalization;
    private Button _button;
    private void Awake()
    {
        (_button = GetComponent<Button>()).onClick.AddListener(OnButtonClickedHandler);
    }
    private void OnButtonClickedHandler()
    {
        LocalizationManager.Instance.SetLocalization(_targetLocalization);
    }
}
