﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BackButton : MonoBehaviour
{
    private Button _button;
    private void Awake()
    {
        (_button = GetComponent<Button>()).onClick.AddListener(OnButtonClickedHandler);
    }
    private void OnButtonClickedHandler()
    {
        BackController.Instance.OnPressBack();
    }
}
