﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ChangeSceneButton : MonoBehaviour {
    public SceneEnum targetScene = SceneEnum.MainMenu;
    private Button _button;
    private void Awake()
    {
        (_button = GetComponent<Button>()).onClick.AddListener(OnButtonClickedHandler);
    }
    private void OnButtonClickedHandler()
    {
        SceneManager.Instance.ChangeScene(targetScene);
    }
}
