﻿using DG.Tweening;
using Game.Extensions;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StarImage
{
    public RectTransform RectTransform { get; set; }
    public Image Image { get; set; }
    public EventTrigger EventTrigger { get; set; }
}

public class RateMePopup : Popup
{
    #region Private members
    private StarImage[] _starImages = null;
    private Blocker _blocker = null;
    private int _starsChosen = 0;
    private int _starsNeeded = 4;
    private int _timesCanBeShown = 3;
    private int _timesShown = 1;
    private bool _wasShown = false;
    private static readonly string _BLOCKER_NAME = "RATE_ME_BLOCKER";

    private static readonly SceneEnum[] _CAN_SHOW_AFTER_SCENES = new SceneEnum[]
    {
        SceneEnum.Map,
        SceneEnum.Game1,
        SceneEnum.Game2,
        SceneEnum.Game3,
        SceneEnum.Game4,
        SceneEnum.Game5,
    };

    [SerializeField] private RectTransform _exitPanelRectTransform = null;
    [SerializeField] private Vector2 _exitPanelActiveAnchorPos = Vector2.zero;
    [SerializeField] private Vector2 _exitPanelInactiveAnchorPos = Vector2.zero;
    [SerializeField] private Text _rateMeText = null;
    [SerializeField] private Button _submitButton = null;
    [SerializeField] private Button _closeButton = null;
    [SerializeField] private Sprite _starActiveSprite = null;
    [SerializeField] private Sprite _starInactiveSprite = null;
    #endregion

    #region Private methods
    private void InitializeStars()
    {
        var starsParent = transform.Find("Stars");
        int count = starsParent.childCount;
        _starImages = new StarImage[count];
        EventTrigger.Entry[] entries = new EventTrigger.Entry[count];
        Transform child = null;
        for (int i = 0; i < count; i++)
        {
            child = starsParent.GetChild(i);
            _starImages[i] = new StarImage()
            {
                Image = child.GetComponent<Image>(),
                RectTransform = child.GetComponent<RectTransform>(),
                EventTrigger = child.GetComponent<EventTrigger>(),
            };
            entries[i] = new EventTrigger.Entry();
            entries[i].eventID = EventTriggerType.PointerClick;
        }
        entries[0].callback.AddListener(OnStar1Clicked);
        _starImages[0].EventTrigger.triggers.Add(entries[0]);

        entries[1].callback.AddListener(OnStar2Clicked);
        _starImages[1].EventTrigger.triggers.Add(entries[1]);

        entries[2].callback.AddListener(OnStar3Clicked);
        _starImages[2].EventTrigger.triggers.Add(entries[2]);

        entries[3].callback.AddListener(OnStar4Clicked);
        _starImages[3].EventTrigger.triggers.Add(entries[3]);

        entries[4].callback.AddListener(OnStar5Clicked);
        _starImages[4].EventTrigger.triggers.Add(entries[4]);
    }
    private void SetButtonsInteractive(bool interactive)
    {
        int count = _starImages.Length;
        for (int i = 0; i < count; i++)
        {
            _starImages[i].EventTrigger.enabled = interactive;
        }
        _closeButton.interactable = interactive;
    }
    private void SetSubmitButtonInteractive(bool interactive)
    {
        _submitButton.interactable = interactive;
    }
    private void ShowStars(int clickedStarIndex)
    {
        int length = _starImages.Length, i = 0;
        for (; i <= clickedStarIndex; i++)
            _starImages[i].Image.sprite = _starActiveSprite;
        for (; i < length; i++)
            _starImages[i].Image.sprite = _starInactiveSprite;
    }
    private void OnLoadingStartedHandler(SceneEnum prevScene, SceneEnum nextScene)
    {
        if (!_wasShown)
            if (_timesShown < _timesCanBeShown)
                if (_CAN_SHOW_AFTER_SCENES.Contains(prevScene))
                    if (PopupManager.Instance.Show(PopupType.RateMe))
                    {
                        _wasShown = true;
                        ++_timesShown;
                        _blocker.IsBlocking = true;
                    }
    }
    private void OnApplicationPausedHandler(bool pause)
    {
        if (pause)
            PlayerPrefsHelper.SetInt(PlayerPrefsKey.TIMES_RATE_ME_SHOWN, _timesShown);
    }
    private void OnApplicationQuitHandler()
    {
        PlayerPrefsHelper.SetInt(PlayerPrefsKey.TIMES_RATE_ME_SHOWN, _timesShown);
    }
    #endregion

    #region Public methods
    public override void Init()
    {
        _timesShown = PlayerPrefsHelper.GetInt(PlayerPrefsKey.TIMES_RATE_ME_SHOWN, 0);
        _exitPanelRectTransform.anchoredPosition = _exitPanelInactiveAnchorPos;
        _submitButton.onClick.AddListener(OnSubmitButtonClicked);
        _closeButton.onClick.AddListener(OnCloseButtonClicked);
        InitializeStars();
        SetButtonsInteractive(false);
        SetSubmitButtonInteractive(false);
        IsShown = false;
        IsInTransition = false;
        gameObject.SetActive(false);
    }
    public override void Subscribe()
    {
        MonoListener.Instance.OnApplicationPauseEvent += OnApplicationPausedHandler;
        MonoListener.Instance.OnApplicationQuitEvent += OnApplicationQuitHandler;
        SceneManager.Instance.OnLoadingStartedEvent += OnLoadingStartedHandler;
    }
    public override void Load()
    {
        _blocker = new Blocker();
        var blockable = SceneManager.Instance as IBlockable;
        blockable.AddBlocker(_BLOCKER_NAME, _blocker);
    }

    public override bool Show()
    {
        if (!base.Show())
            return false;
        IsInTransition = true;
        IsShown = true;
        gameObject.SetActive(true);
        _starsChosen = 0;
        ShowStars(-1);
        SetButtonsInteractive(false);
        SetSubmitButtonInteractive(false);
        _exitPanelRectTransform.DOAnchorPos(_exitPanelActiveAnchorPos, 0.5f).OnComplete(() =>
        {
            IsInTransition = false;
            SetButtonsInteractive(true);
        });
        return true;
    }
    public override bool Hide()
    {
        if (!base.Hide())
            return false;
        IsInTransition = true;
        IsShown = false;
        SetButtonsInteractive(false);
        SetSubmitButtonInteractive(false);
        _exitPanelRectTransform.DOAnchorPos(_exitPanelInactiveAnchorPos, 0.5f).OnComplete(() =>
        {
            _blocker.IsBlocking = false;
            IsInTransition = false;
            gameObject.SetActive(false);
        });
        return true;
    }
    #endregion

    #region Button handlers
    private void OnStar1Clicked(BaseEventData baseEventData)
    {
        OnStarClicked(0);
    }
    private void OnStar2Clicked(BaseEventData baseEventData)
    {
        OnStarClicked(1);
    }
    private void OnStar3Clicked(BaseEventData baseEventData)
    {
        OnStarClicked(2);
    }
    private void OnStar4Clicked(BaseEventData baseEventData)
    {
        OnStarClicked(3);
    }
    private void OnStar5Clicked(BaseEventData baseEventData)
    {
        OnStarClicked(4);
    }
    private void OnStarClicked(int clickedStarIndex)
    {
        _starsChosen = clickedStarIndex + 1;
        ShowStars(clickedStarIndex);
        SetSubmitButtonInteractive(true);
    }
    private void OnSubmitButtonClicked()
    {
        PopupManager.Instance.Hide(PopupType.RateMe);
        if (_starsChosen >= _starsNeeded)
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.atgames.memesboard");
    }
    private void OnCloseButtonClicked()
    {
        PopupManager.Instance.Hide(PopupType.RateMe);
    }
    #endregion
}
