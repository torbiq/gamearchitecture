﻿using System;
using UnityEngine;

public enum PopupType
{
    Quit,
    RateMe,
}

public abstract class Popup : MonoBehaviour
{
    #region Events
    public event Action<float> OnShowStartedEvent;
    public event Action<float> OnHideStartedEvent;
    #endregion

    [SerializeField] protected float _showDuration = 0.5f;
    [SerializeField] protected float _hideDuration = 0.5f;
    [SerializeField] protected bool _canStayWhenSwitchingScene = false;
    [SerializeField] protected bool _canShowDuringTransition = true;
    [SerializeField] PopupType _type;

    public float ShowDuration { get { return _showDuration; } }
    public float HideDuration { get { return _hideDuration; } }
    public bool CanStayWhenSwitchingScene { get { return _canStayWhenSwitchingScene; } }
    public bool CanShowDuringTransition { get { return _canShowDuringTransition; } }
    public PopupType Type { get { return _type; } }

    public bool IsShown { get; protected set; }
    public bool IsInTransition { get; protected set; }
    
    public virtual void Init()
    {
    }
    public virtual void Subscribe()
    {
    }
    public virtual void Load()
    {
    }

    public virtual bool Show()
    {
        if (IsInTransition)
            return false;
        if (OnShowStartedEvent != null)
            OnShowStartedEvent(_showDuration);
        return true;
    }
    public virtual bool Hide()
    {
        if (IsInTransition)
            return false;
        if (OnHideStartedEvent != null)
            OnHideStartedEvent(_hideDuration);
        return true;
    }
}
