﻿using DG.Tweening;
using Game.Extensions;
using System;
using UnityEngine;
using UnityEngine.UI;

public class QuitPopup : Popup
{
    #region Private members
    [SerializeField] private Vector2 _exitPanelActiveAnchorPos = new Vector2();
    [SerializeField] private Vector2 _exitPanelInactiveAnchorPos = new Vector2();
    [SerializeField] private RectTransform _exitPanelRectTransform = null;
    [SerializeField] private Text _quitText = null;
    [SerializeField] private Button _yesButton = null;
    [SerializeField] private Button _noButton = null;
    [SerializeField] private Button _closeButton = null;
    #endregion

    #region Private methods
    private void SetButtonsInteractive(bool interactive)
    {
        _yesButton.interactable = interactive;
        _noButton.interactable = interactive;
        _closeButton.interactable = interactive;
    }
    #endregion

    #region Public methods
    public override void Init()
    {
        _exitPanelRectTransform.anchoredPosition = _exitPanelInactiveAnchorPos;
        _yesButton.onClick.AddListener(OnYesButtonClicked);
        _noButton.onClick.AddListener(OnNoButtonClicked);
        _closeButton.onClick.AddListener(OnCloseButtonClicked);
        SetButtonsInteractive(false);
        IsShown = false;
        IsInTransition = false;
        gameObject.SetActive(false);
    }
    public override bool Show()
    {
        if (!base.Show())
            return false;
        IsInTransition = true;
        IsShown = true;
        gameObject.SetActive(true);
        SetButtonsInteractive(false);
        _exitPanelRectTransform.DOAnchorPos(_exitPanelActiveAnchorPos, 0.5f).OnComplete(() =>
        {
            IsInTransition = false;
            SetButtonsInteractive(true);
        });
        return true;
    }
    public override bool Hide()
    {
        if (!base.Hide())
            return false;
        IsInTransition = true;
        IsShown = false;
        SetButtonsInteractive(false);
        _exitPanelRectTransform.DOAnchorPos(_exitPanelInactiveAnchorPos, 0.5f).OnComplete(() =>
        {
            IsInTransition = false;
            gameObject.SetActive(false);
            SetButtonsInteractive(true);
        });
        return true;
    }
    #endregion

    #region Button handlers
    private void OnYesButtonClicked()
    {
        Application.Quit();
    }
    private void OnNoButtonClicked()
    {
        PopupManager.Instance.Hide(PopupType.Quit);
    }
    private void OnCloseButtonClicked()
    {
        PopupManager.Instance.Hide(PopupType.Quit);
    }
    #endregion
}
