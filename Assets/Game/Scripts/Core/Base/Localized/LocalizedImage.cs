﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.Extensions;

[RequireComponent(typeof(Image))]
public class LocalizedImage : MonoBehaviour
{
    [SerializeField] private string _spriteName = "";
    [SerializeField] private Sprite _defaultSprite = null;
    private Image _image;
    private void Awake()
    {
        _image = GetComponent<Image>();
    }
    private void OnEnable()
    {
        LocalizationManager.Instance.OnLocalizationLoadedEvent += OnLocalizationLoadedHandler;
        OnLocalizationLoadedHandler(LocalizationManager.Instance.GetLocalization());
    }
    private void OnDisable()
    {
        if (LocalizationManager.Instance != null)
            LocalizationManager.Instance.OnLocalizationLoadedEvent -= OnLocalizationLoadedHandler;
    }
    private void OnLocalizationLoadedHandler(Localization localization)
    {
        _image.sprite = GetSprite(localization);
    }
    private Sprite GetSprite(Localization localization)
    {
        if (ResourceManager.Instance.LocalizedPermanentResourcesHandler.Sprites != null)
        {
            var foundSprite = ResourceManager.Instance.LocalizedPermanentResourcesHandler.Sprites.GetValueOrDefault(_spriteName);
            if (!foundSprite)
                if (ResourceManager.Instance.LocalizedSceneResourcesHandler.Sprites != null)
                {
                    foundSprite = ResourceManager.Instance.LocalizedSceneResourcesHandler.Sprites.GetValueOrDefault(_spriteName);
                    if (!foundSprite)
                        return _defaultSprite;
                }
                else
                    return _defaultSprite;
            return foundSprite;
        }
        return _defaultSprite;
    }
}
