﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.Extensions;

[RequireComponent(typeof(Text))]
public class LocalizedText : MonoBehaviour {
    [SerializeField] private string _phraze = "";
    [SerializeField] private string _defaultString = "";
    private Text _text;
    private void Awake()
    {
        _text = GetComponent<Text>();
    }
    private void OnEnable()
    {
        LocalizationManager.Instance.OnLocalizationLoadedEvent += OnLocalizationLoadedHandler;
        OnLocalizationLoadedHandler(LocalizationManager.Instance.GetLocalization());
    }
    private void OnDisable()
    {
        if (LocalizationManager.Instance != null)
            LocalizationManager.Instance.OnLocalizationLoadedEvent -= OnLocalizationLoadedHandler;
    }
    private void OnLocalizationLoadedHandler(Localization localization)
    {
        _text.text = LocalizationManager.Instance.Phrazes.GetValueOrDefault(_phraze, _defaultString);
    }
}
