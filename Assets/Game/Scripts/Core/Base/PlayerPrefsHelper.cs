﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public enum PlayerPrefsKey
{
    KEY_LANGUAGE,
    KEY_AGE,
    TIMES_RATE_ME_SHOWN,
}

public static class PlayerPrefsHelper
{
#if UNITY_EDITOR
    [MenuItem("Helpers/Clear Player Prefs")]
    public static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
#endif

    #region Public methods
    #region Getters
    public static string GetString(PlayerPrefsKey key)
    {
        return PlayerPrefs.GetString(key.ToString());
    }
    public static string GetString(PlayerPrefsKey key, string defaultValue)
    {
        return PlayerPrefs.GetString(key.ToString(), defaultValue);
    }

    public static int GetInt(PlayerPrefsKey key)
    {
        return PlayerPrefs.GetInt(key.ToString());
    }
    public static int GetInt(PlayerPrefsKey key, int defaultValue)
    {
        return PlayerPrefs.GetInt(key.ToString(), defaultValue);
    }

    public static float GetFloat(PlayerPrefsKey key)
    {
        return PlayerPrefs.GetFloat(key.ToString());
    }
    public static float GetFloat(PlayerPrefsKey key, float defaultValue)
    {
        return PlayerPrefs.GetFloat(key.ToString(), defaultValue);
    }
    #endregion

    #region Setters
    public static void SetString(PlayerPrefsKey key, string defaultValue)
    {
        PlayerPrefs.SetString(key.ToString(), defaultValue);
    }
    public static void SetInt(PlayerPrefsKey key, int defaultValue)
    {
        PlayerPrefs.SetInt(key.ToString(), defaultValue);
    }
    public static void SetFloat(PlayerPrefsKey key, float defaultValue)
    {
        PlayerPrefs.SetFloat(key.ToString(), defaultValue);
    }
    #endregion
    #endregion
}
