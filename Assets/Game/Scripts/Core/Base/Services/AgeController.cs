﻿using System;
using System.Collections.Generic;

public enum Age
{
    AGE_FIVE_PLUS,
    AGE_TWO_PLUS,
    AGE_NO_RATING,
}

public interface IAgeController
{
    event Action<Age> OnAgeChangedEvent;
    bool IsAge(Age age);
    void SetAge(Age age);
    Age GetAge();
}

public class AgeController : ASingletoneService<AgeController, IAgeController>, IAgeController
{
    #region Private members
    private readonly Dictionary<SceneEnum, Age> _AGE_OVERRIDE_SCENES = new Dictionary<SceneEnum, Age>
    {
    };
    private readonly Age _AGE_DEFAULT = Age.AGE_TWO_PLUS;
    private Age _age;
    #endregion

    #region Events
    public event Action<Age> OnAgeChangedEvent;
    #endregion

    #region Private methods
    private Age GetAgeStored()
    {
        return (Age)PlayerPrefsHelper.GetInt(PlayerPrefsKey.KEY_AGE, (int)_AGE_DEFAULT);
    }
    private void SetAgeStored(Age age)
    {
        PlayerPrefsHelper.SetInt(PlayerPrefsKey.KEY_AGE, (int)age);
    }
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        _age = GetAgeStored();
    }
    #endregion

    #region Core
    public bool IsAge(Age age)
    {
        Age currentAge = _age;
        _AGE_OVERRIDE_SCENES.TryGetValue(SceneManager.GetCurrentSceneFromUnity(), out currentAge);
        return currentAge == age;
    }
    public void SetAge(Age age)
    {
        if (_AGE_OVERRIDE_SCENES.ContainsKey(SceneManager.GetCurrentSceneFromUnity()))
        {
            Log.Error("Can't change age for this level because it's overrided.");
            return;
        }
        if (!Enum.IsDefined(typeof(Age), age))
        {
            Log.Error("Chosen age [value equals = " + age + "] is not defined in enum list.");
            return;
        }
        _age = age;
        SetAgeStored(_age);
        if (OnAgeChangedEvent != null)
        {
            OnAgeChangedEvent(_age);
        }
    }
    public Age GetAge()
    {
        Age age;
        if (_AGE_OVERRIDE_SCENES.TryGetValue(SceneManager.GetCurrentSceneFromUnity(), out age))
        {
            return age;
        }
        return _age;
    }
    #endregion
    #endregion
}
