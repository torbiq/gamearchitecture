﻿using System;
using System.Collections;
using UnityEngine;

public interface IMonoListener
{
    event Action OnUpdateEvent;
    event Action OnFixedUpdateEvent;
    event Action OnLateUpdateEvent;
    event Action<bool> OnApplicationFocusEvent;
    event Action<bool> OnApplicationPauseEvent;
    event Action OnApplicationQuitEvent;

    Coroutine StartCoroutine(IEnumerator routine);
    void StopCoroutine(IEnumerator routine);
}

public class MonoListener : ASingletoneService<MonoListener, IMonoListener>, IMonoListener
{
    #region Events
    public event Action OnUpdateEvent;
    public event Action OnFixedUpdateEvent;
    public event Action OnLateUpdateEvent;
    public event Action<bool> OnApplicationFocusEvent;
    public event Action<bool> OnApplicationPauseEvent;
    public event Action OnApplicationQuitEvent;
    #endregion

    #region Unity
    private void Update()
    {
        if (OnUpdateEvent != null)
            OnUpdateEvent();
    }
    private void FixedUpdate()
    {
        if (OnFixedUpdateEvent != null)
            OnFixedUpdateEvent();
    }
    private void LateUpdate()
    {
        if (OnLateUpdateEvent != null)
            OnLateUpdateEvent();
    }
    private void OnApplicationFocus(bool focus)
    {
        if (OnApplicationFocusEvent != null)
            OnApplicationFocusEvent(focus);
    }
    private void OnApplicationPause(bool pause)
    {
        if (OnApplicationPauseEvent != null)
            OnApplicationPauseEvent(pause);
    }
    private void OnApplicationQuit()
    {
        if (OnApplicationQuitEvent != null)
            OnApplicationQuitEvent();
    }
    #endregion

    #region Public methods
    #region Core
    public new Coroutine StartCoroutine(IEnumerator routine)
    {
        return base.StartCoroutine(routine);
    }
    public new void StopCoroutine(IEnumerator routine)
    {
        base.StopCoroutine(routine);
    }
    #endregion
    #endregion
}
