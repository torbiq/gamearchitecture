﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using Game.Extensions;

public interface IPopupManager
{
    event Action<Popup> OnShown;
    event Action<Popup> OnHidden;

    bool Show(PopupType popupType);
    bool Hide(PopupType popupType);
    void HideAll();
    bool IsShown(PopupType popupType);
}

public class PopupManager : ASingletoneService<PopupManager, IPopupManager>, IPopupManager
{
    #region Events
    public event Action<Popup> OnShown;
    public event Action<Popup> OnHidden;
    #endregion

    #region Private members
    private Dictionary<PopupType, Popup> _popups;
    private List<Popup> _popupsEnabled;
    private bool _isBackgroundImageShown = false;
    [SerializeField] private Image _backgroundImage;
    #endregion

    #region Event handlers
    private void OnStartTransitionHandler(SceneEnum prevScene, SceneEnum nextScene)
    {
        int count = _popupsEnabled.Count;
        for (int i = 0; i < count; i++)
            if (!_popupsEnabled[i].CanStayWhenSwitchingScene)
                Hide(_popupsEnabled[i].Type);
    }
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        _popups = new Dictionary<PopupType, Popup>();
        _popups.Add(PopupType.Quit, transform.Find("QuitPanel").GetComponent<QuitPopup>());
        _popups.Add(PopupType.RateMe, transform.Find("RateMePanel").GetComponent<RateMePopup>());
        foreach (var kvp in _popups)
            kvp.Value.Init();
        _popupsEnabled = new List<Popup>();
        _backgroundImage.color = _backgroundImage.color.GetWithAlpha(0.0f);
        _backgroundImage.gameObject.SetActive(false);
        _isBackgroundImageShown = false;
    }
    public override void Subscribe()
    {
        SceneManager.Instance.OnStartTransitionEvent += OnStartTransitionHandler;
        foreach (var kvp in _popups)
            kvp.Value.Subscribe();
    }
    public override void Load()
    {
        foreach (var kvp in _popups)
            kvp.Value.Load();
    }
    #endregion

    private void ShowBackground()
    {
        _backgroundImage.gameObject.SetActive(true);
        _backgroundImage.DOKill(false);
        _backgroundImage.DOFade(0.5f, 0.5f);
        _isBackgroundImageShown = true;
    }
    private void HideBackground()
    {
        _backgroundImage.DOKill(false);
        _backgroundImage.DOFade(0.0f, 0.5f).OnComplete(()=> {
            _backgroundImage.gameObject.SetActive(false);
        });
        _isBackgroundImageShown = false;
    }

    #region Core
    public bool Show(PopupType popupType)
    {
        var popup = _popups[popupType];
        if (SceneManager.Instance.IsInTransition)
            if (!popup.CanShowDuringTransition)
            {
                return false;
            }
        if (popup.Show())
        {
            _popupsEnabled.Add(popup);
            if (OnShown != null)
                OnShown(popup);
            if (!_isBackgroundImageShown)
                ShowBackground();
            return true;
        }
        return false;
    }
    public bool Hide(PopupType popupType)
    {
        var popup = _popups[popupType];
        if (popup.Hide())
        {
            _popupsEnabled.Remove(popup);
            if (OnHidden != null)
                OnHidden(popup);
            if (_isBackgroundImageShown && _popupsEnabled.Count == 0)
                HideBackground();
            return true;
        }
        return false;
    }
    public void HideAll()
    {
        foreach (var kvp in _popups)
            if (kvp.Value.IsShown)
            {
                _popupsEnabled.Remove(kvp.Value);
                kvp.Value.Hide();
                if (OnHidden != null)
                    OnHidden(kvp.Value);
                if (_isBackgroundImageShown && _popupsEnabled.Count == 0)
                    HideBackground();
            }
    }
    public bool IsShown(PopupType popupType)
    {
        return _popups[popupType].IsShown;
    }
    #endregion
    #endregion
}
