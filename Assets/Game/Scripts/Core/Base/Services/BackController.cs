﻿using DG.Tweening;
using Game.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IBackController
{
    event Action<SceneEnum> OnSceneSwitchedBackEvent;

    bool IsSceneBackable();
    void OnPressBack();
}

public class BackController : ASingletoneService<BackController, IBackController>, IBackController
{
    #region Events
    public event Action<SceneEnum> OnSceneSwitchedBackEvent;
    #endregion
    
    #region Private members
    private static readonly Dictionary<SceneEnum, SceneEnum?> _SCENE_SWITCHES = new Dictionary<SceneEnum, SceneEnum?> {
        { SceneEnum.EntryPoint, null },
        { SceneEnum.MainMenu, null },
        { SceneEnum.Map, SceneEnum.MainMenu },
        { SceneEnum.Settings, SceneEnum.MainMenu },
        { SceneEnum.AboutUs, SceneEnum.MainMenu },
    };
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
    }
    public override void Subscribe()
    {
        KeyListener.Instance.AddKeyAction(KeyActionType.DOWN, KeyCode.Escape, OnPressBack);
    }
    #endregion

    #region Core
    public bool IsSceneBackable()
    {
        return _SCENE_SWITCHES.GetValueOrDefault(SceneManager.Instance.GetCurrentScene()) != null;
    }
    public void OnPressBack()
    {
        var nextLevelFromDict = _SCENE_SWITCHES.GetValueOrDefault(SceneManager.Instance.GetCurrentScene());
        if (nextLevelFromDict == null)
        {
            if (PopupManager.Instance.IsShown(PopupType.Quit))
                PopupManager.Instance.Hide(PopupType.Quit);
            else
                PopupManager.Instance.Show(PopupType.Quit);
        }
        else
        {
            SceneManager.Instance.ChangeScene(nextLevelFromDict.Value);
            if (OnSceneSwitchedBackEvent != null)
                OnSceneSwitchedBackEvent(nextLevelFromDict.Value);
        }
    }
    #endregion
    #endregion
}
