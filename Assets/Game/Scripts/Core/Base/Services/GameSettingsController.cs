﻿using Game.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameSettingsController
{
    bool AdsEnabled { get; set; }
    float MusicVolume { get; set; }
    bool MusicEnabled { get; set; }
    float SfxVolume { get; set; }
    bool SfxEnabled { get; set; }
}

public class GameSettingsController : ASingletoneService<GameSettingsController, IGameSettingsController>, IGameSettingsController
{
    #region Private members
    private static readonly string _KEY_ADS_ENABLED = "ADS_ENABLED";
    private static readonly string _KEY_MUSIC_VOLUME = "MUSIC_VOLUME";
    private static readonly string _KEY_MUSIC_ENABLED = "MUSIC_ENABLED";
    private static readonly string _KEY_SFX_VOLUME = "SFX_VOLUME";
    private static readonly string _KEY_SFX_ENABLED = "SFX_ENABLED";

    private bool _adsEnabled = false;
    private float _musicVolume = 1.0f;
    private bool _musicEnabled = false;
    private float _sfxVolume = 1.0f;
    private bool _sfxEnabled = false;

    private static readonly bool _DEFAULT_ADS_ENABLED = true;
    private static readonly float _DEFAULT_MUSIC_VOLUME = 1.0f;
    private static readonly bool _DEFAULT_MUSIC_ENABLED = true;
    private static readonly float _DEFAULT_SFX_VOLUME = 1.0f;
    private static readonly bool _DEFAULT_SFX_ENABLED = true;
    #endregion

    #region Properties
    public bool AdsEnabled { get { return _adsEnabled; } set { _adsEnabled = value; } }
    public float MusicVolume { get { return _musicVolume; } set { _musicVolume = value; } }
    public bool MusicEnabled { get { return _musicEnabled; } set { _musicEnabled = value; } }
    public float SfxVolume { get { return _sfxVolume; } set { _sfxVolume = value; } }
    public bool SfxEnabled { get { return _sfxEnabled; } set { _sfxEnabled = value; } }
    #endregion

    #region Event handlers
    private void OnApplicationQuitHandler()
    {
        PlayerPrefs.SetInt(_KEY_ADS_ENABLED, _adsEnabled.ToInt());
        PlayerPrefs.SetFloat(_KEY_MUSIC_VOLUME, _musicVolume);
        PlayerPrefs.SetInt(_KEY_MUSIC_ENABLED, _musicEnabled.ToInt());
        PlayerPrefs.SetFloat(_KEY_SFX_VOLUME, _sfxVolume);
        PlayerPrefs.SetInt(_KEY_SFX_ENABLED, _sfxEnabled.ToInt());
    }
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        _adsEnabled = PlayerPrefs.GetInt(_KEY_ADS_ENABLED, _DEFAULT_ADS_ENABLED.ToInt()).ToBool();
        _musicVolume = PlayerPrefs.GetFloat(_KEY_MUSIC_VOLUME, _DEFAULT_MUSIC_VOLUME);
        _musicEnabled = PlayerPrefs.GetInt(_KEY_MUSIC_ENABLED, _DEFAULT_MUSIC_ENABLED.ToInt()).ToBool();
        _sfxVolume = PlayerPrefs.GetFloat(_KEY_SFX_VOLUME, _DEFAULT_SFX_VOLUME);
        _sfxEnabled = PlayerPrefs.GetInt(_KEY_SFX_ENABLED, _DEFAULT_SFX_ENABLED.ToInt()).ToBool();
    }
    public override void Subscribe()
    {
        MonoListener.Instance.OnApplicationQuitEvent += OnApplicationQuitHandler;
    }
    public override void Load()
    {
    }
    #endregion
    #endregion
}