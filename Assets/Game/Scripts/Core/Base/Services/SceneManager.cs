﻿#define USE_DEBUG_MODE

using DG.Tweening;
using Game.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

#region Enums
public enum SceneEnum
{
    EntryPoint,
    MainMenu,
    Settings,
    AboutUs,
    Map,
    Game1,
    Game2,
    Game3,
    Game4,
    Game5,
}
public enum SceneGroup
{
    Menu,
    Game,
}
#endregion

#region Blocking
public class Blocker
{
    public bool IsBlocking { get; set; }
}
public interface IBlockable
{
    void AddBlocker(string name, Blocker blocker);
    Blocker GetBlocker(string name);
    bool RemoveBlocker(string name);
    bool GetIsBlocked();
}
#endregion

public interface ISceneManager
{
    event Action<SceneEnum, SceneEnum> OnStartTransitionEvent;
    event Action<float> OnWaitingBeforeLoadEvent;
    event Action<SceneEnum, SceneEnum> OnLoadingStartedEvent;
    event Action<float> OnProgressUpdateEvent;
    event Action<SceneEnum> OnPreSceneLoadedEvent;
    event Action<SceneEnum> OnSceneLoadedEvent;
    event Action<float> OnWaitingBeforeShowEvent;
    event Action OnShowSceneEvent;

    float WaitingBeforeLoadDelay { get; }
    float WaitingBeforeShowDelay { get; }
    bool IsInTransition { get; }

    void ChangeScene(SceneEnum scene);
    SceneEnum GetCurrentScene();
}

public class SceneManager : ASingletoneService<SceneManager, ISceneManager>, ISceneManager, IBlockable
{
    #region Events
    public event Action<SceneEnum, SceneEnum> OnStartTransitionEvent;
    public event Action<float> OnWaitingBeforeLoadEvent;
    public event Action<SceneEnum, SceneEnum> OnLoadingStartedEvent;
    public event Action<float> OnProgressUpdateEvent;
    public event Action<SceneEnum> OnPreSceneLoadedEvent;
    public event Action<SceneEnum> OnSceneLoadedEvent;
    public event Action<float> OnWaitingBeforeShowEvent;
    public event Action OnShowSceneEvent;
    #endregion

    #region Properties
    public float WaitingBeforeLoadDelay { get { return _waitingBeforeLoadDelay; } }
    public float WaitingBeforeShowDelay { get { return _waitingBeforeShowDelay; } }

    public bool IsInTransition { get { return _isIntransition; } }
    #endregion

    #region Static methods
    public static SceneEnum GetCurrentSceneFromUnity()
    {
        return Extensions.ParseStringToEnum<SceneEnum>(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
    #endregion

    #region Private members
    [SerializeField] private float _waitingBeforeLoadDelay = 1.0f;
    [SerializeField] private float _waitingBeforeShowDelay = 0.5f;

    private bool _isIntransition = false;
    private SceneEnum _currentScene = SceneEnum.EntryPoint;
    private AsyncOperation _loadingSceneAsyncOperation = null;
    private Dictionary<string, Blocker> _blockersDict = new Dictionary<string, Blocker>();
    
    private static readonly Dictionary<SceneEnum, SceneGroup> _SCENE_GROUPS = new Dictionary<SceneEnum, SceneGroup> {
        { SceneEnum.EntryPoint, SceneGroup.Menu },
        { SceneEnum.Settings, SceneGroup.Menu },
        { SceneEnum.AboutUs, SceneGroup.Menu },

        { SceneEnum.Map, SceneGroup.Game },
        { SceneEnum.Game1, SceneGroup.Game },
        { SceneEnum.Game2, SceneGroup.Game },
        { SceneEnum.Game3, SceneGroup.Game },
        { SceneEnum.Game4, SceneGroup.Game },
        { SceneEnum.Game5, SceneGroup.Game },
    };
    private static readonly SceneGroup _DEFAULT_SCENE_GROUP = SceneGroup.Menu;
    #endregion

    #region Public methods
    #region Service Implementation
    public override void Init()
    {
        _currentScene = GetCurrentSceneFromUnity();
    }
    #endregion

    #region Core Implementation
    public SceneEnum GetCurrentScene()
    {
        return _currentScene;
    }
    public void ChangeScene(SceneEnum scene)
    {
        if (_isIntransition)
        {
            Log.Error("Transition is running.");
            return;
        }
        _isIntransition = true;
        if (OnStartTransitionEvent != null)
            OnStartTransitionEvent(_currentScene, scene);
        WaitBeforeLoad(scene);
    }
    #endregion

    #region IsBlockable Implementation
    public void AddBlocker(string name, Blocker blocker)
    {
        _blockersDict.Add(name, blocker);
    }
    public Blocker GetBlocker(string name)
    {
        return _blockersDict.GetValueOrDefault(name);
    }
    public bool RemoveBlocker(string name)
    {
        return _blockersDict.Remove(name);
    }
    public bool GetIsBlocked()
    {
        foreach (var kvp in _blockersDict)
            if (kvp.Value.IsBlocking)
                return true;
        return false;
    }
    #endregion
    #endregion

    #region Private methods
    private SceneGroup GetSceneGroup(SceneEnum scene)
    {
        return _SCENE_GROUPS.GetValueOrDefault(scene, _DEFAULT_SCENE_GROUP);
    }
    private Tween WaitBeforeLoad(SceneEnum scene)
    {
        if (OnWaitingBeforeLoadEvent != null)
            OnWaitingBeforeLoadEvent(_waitingBeforeLoadDelay);
        return DOVirtual.DelayedCall(_waitingBeforeLoadDelay, () => {
            if (OnLoadingStartedEvent != null)
                OnLoadingStartedEvent(_currentScene, scene);
            StartCoroutine(LoadSceneAsync(scene, WaitingBeforeShow));
        });
    }
    private void WaitingBeforeShow()
    {
        if (OnWaitingBeforeShowEvent != null)
            OnWaitingBeforeShowEvent(_waitingBeforeShowDelay);
        DOVirtual.DelayedCall(_waitingBeforeShowDelay, () =>
        {
            _isIntransition = false;
            if (OnShowSceneEvent != null)
                OnShowSceneEvent();
        });
    }
    private IEnumerator LoadSceneAsync(SceneEnum scene, Action OnCompleteCallback = null)
    {
        _loadingSceneAsyncOperation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(scene.ToString(), LoadSceneMode.Single);
        _loadingSceneAsyncOperation.allowSceneActivation = !GetIsBlocked();
        do
        {
            yield return null;
            if (OnProgressUpdateEvent != null)
                OnProgressUpdateEvent(_loadingSceneAsyncOperation.progress);
            _loadingSceneAsyncOperation.allowSceneActivation = !GetIsBlocked();
        }
        while (!_loadingSceneAsyncOperation.isDone);
        _currentScene = scene;
        if (OnCompleteCallback != null)
            OnCompleteCallback();
        if (OnPreSceneLoadedEvent != null)
            OnPreSceneLoadedEvent(scene);
        if (OnSceneLoadedEvent != null)
            OnSceneLoadedEvent(scene);
    }
    #endregion
}
