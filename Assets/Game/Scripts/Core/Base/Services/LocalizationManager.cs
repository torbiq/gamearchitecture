﻿#define USE_DEBUG_MODE

using DG.Tweening;
using Game.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Localization
{
    Arabic = SystemLanguage.Arabic,
    Chinese = SystemLanguage.Chinese,
    English = SystemLanguage.English,
    French = SystemLanguage.French,
    German = SystemLanguage.German,
    Italian = SystemLanguage.Italian,
    Polish = SystemLanguage.Polish,
    Portuguese = SystemLanguage.Portuguese,
    Russian = SystemLanguage.Russian,
    Spanish = SystemLanguage.Spanish,
}

public interface ILocalizationManager
{
    event Action<Localization> OnLocalizationChangedEvent;
    event Action<Localization> OnLocalizationLoadedEvent;

    Dictionary<string, string> Phrazes { get; }

    Localization GetLocalization();
    void SetLocalization(Localization localization);
}

public class LocalizationManager : ASingletoneService<LocalizationManager, ILocalizationManager>, ILocalizationManager
{
    #region Events
    public event Action<Localization> OnLocalizationChangedEvent;
    public event Action<Localization> OnLocalizationLoadedEvent;
    #endregion

    #region Event handlers
    private void OnApplicationPauseHandler(bool pause)
    {
        if (pause)
            PlayerPrefsHelper.SetInt(PlayerPrefsKey.KEY_LANGUAGE, (int)_localization);
    }
    private void OnApplicationQuitHandler()
    {
        PlayerPrefsHelper.SetInt(PlayerPrefsKey.KEY_LANGUAGE, (int)_localization);
    }
    #endregion

    #region Properties
    public Dictionary<string, string> Phrazes { get; private set; }
    #endregion

    #region Private members
    private Localization _localization;
    private static readonly Localization _DEFAULT_LOCALIZATION = Localization.English;
    private static readonly Localization[] supportedLocalizations = new Localization[]
    {
        Localization.English,
        Localization.Russian,
    };
    #endregion

    #region Private methods
    private void LoadLocalization()
    {
        string converted = System.Text.Encoding.UTF8.GetString(ResourceManager.Instance.LocalizedPermanentResourcesHandler.TextAssets["PermanentPhrazes"].bytes);
        Phrazes = JsonConvert.DeserializeObject<Dictionary<string, string>>(converted, new JsonSerializerSettings()
        {
            Formatting = Formatting.Indented,
        });
        if (OnLocalizationLoadedEvent != null)
            OnLocalizationLoadedEvent(_localization);
    }
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        _localization = (Localization)PlayerPrefsHelper.GetInt(PlayerPrefsKey.KEY_LANGUAGE, (int)Application.systemLanguage);
        if (!supportedLocalizations.Contains(_localization))
            _localization = _DEFAULT_LOCALIZATION;
        Debug.Log("Current localization is: " + _localization);
    }
    public override void Subscribe()
    {
        MonoListener.Instance.OnApplicationPauseEvent += OnApplicationPauseHandler;
        MonoListener.Instance.OnApplicationQuitEvent += OnApplicationQuitHandler;
    }
    public override void Load()
    {
        LoadLocalization();
    }
    #endregion

    #region Core
    public Localization GetLocalization()
    {
        return _localization;
    }
    public void SetLocalization(Localization localization)
    {
        _localization = localization;
        if (OnLocalizationChangedEvent != null)
            OnLocalizationChangedEvent(_localization);
        LoadLocalization();
        Debug.Log("Localization changed to: " + _localization);
    }
    #endregion
    #endregion
}
