﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Extensions;
using System.IO;

#region Resource Handling
public class ResourcesToLoad
{
    public string ParentFolder { get; set; }
    public List<string> Sprites { get; set; }
    public List<string> AudioClips { get; set; }
    public List<string> Prefabs { get; set; }
    public List<string> Textures { get; set; }
    public List<string> OtherObjects { get; set; }
    public List<string> TextAssets { get; set; }
}
public class ResourcesHandler
{
    private Dictionary<string, Sprite> _sprites;
    private Dictionary<string, AudioClip> _audioClips;
    private Dictionary<string, GameObject> _prefabs;
    private Dictionary<string, Texture> _textures;
    private Dictionary<string, UnityEngine.Object> _otherObjects;
    private Dictionary<string, TextAsset> _textAssets;

    public Dictionary<string, Sprite> Sprites { get { return _sprites; } set { _sprites = value; } }
    public Dictionary<string, AudioClip> AudioClips { get { return _audioClips; } set { _audioClips = value; } }
    public Dictionary<string, GameObject> Prefabs { get { return _prefabs; } set { _prefabs = value; } }
    public Dictionary<string, Texture> Textures { get { return _textures; } set { _textures = value; } }
    public Dictionary<string, UnityEngine.Object> OtherObjects { get { return _otherObjects; } set { _otherObjects = value; } }
    public Dictionary<string, TextAsset> TextAssets { get { return _textAssets; } set { _textAssets = value; } }
    public void Unload()
    {
        UnloadResources(Sprites);
        UnloadResources(AudioClips);
        UnloadResources(Prefabs);
        UnloadResources(Textures);
        UnloadResources(OtherObjects);
        UnloadResources(TextAssets);
    }
    public void LoadAsync(ResourcesToLoad resourcesToLoad)
    {
        LoadResourcesAsync(resourcesToLoad.ParentFolder + ResourceManager.SPRITES_PATH, resourcesToLoad.Sprites, ref _sprites);
        LoadResourcesAsync(resourcesToLoad.ParentFolder + ResourceManager.AUDIO_CLIPS_PATH, resourcesToLoad.AudioClips, ref _audioClips);
        LoadResourcesAsync(resourcesToLoad.ParentFolder + ResourceManager.PREFABS_PATH, resourcesToLoad.Prefabs, ref _prefabs);
        LoadResourcesAsync(resourcesToLoad.ParentFolder + ResourceManager.TEXTURES_PATH, resourcesToLoad.Textures, ref _textures);
        LoadResourcesAsync(resourcesToLoad.ParentFolder + ResourceManager.OTHER_OBJECTS_PATH, resourcesToLoad.OtherObjects, ref _otherObjects);
        LoadResourcesAsync(resourcesToLoad.ParentFolder + ResourceManager.TEXT_ASSETS_PATH, resourcesToLoad.TextAssets, ref _textAssets);
    }
    public void Load(ResourcesToLoad resourcesToLoad)
    {
        LoadResources(resourcesToLoad.ParentFolder + ResourceManager.SPRITES_PATH, resourcesToLoad.Sprites, ref _sprites);
        LoadResources(resourcesToLoad.ParentFolder + ResourceManager.AUDIO_CLIPS_PATH, resourcesToLoad.AudioClips, ref _audioClips);
        LoadResources(resourcesToLoad.ParentFolder + ResourceManager.PREFABS_PATH, resourcesToLoad.Prefabs, ref _prefabs);
        LoadResources(resourcesToLoad.ParentFolder + ResourceManager.TEXTURES_PATH, resourcesToLoad.Textures, ref _textures);
        LoadResources(resourcesToLoad.ParentFolder + ResourceManager.OTHER_OBJECTS_PATH, resourcesToLoad.OtherObjects, ref _otherObjects);
        LoadResources(resourcesToLoad.ParentFolder + ResourceManager.TEXT_ASSETS_PATH, resourcesToLoad.TextAssets, ref _textAssets);
    }
    private void UnloadResources<T>(Dictionary<string, T> list) where T : UnityEngine.Object
    {
        if (list != null)
        {
            foreach(var kvp in list)
                Resources.UnloadAsset(kvp.Value);
            list.Clear();
        }
    }
    private void LoadResources<T>(string parentFolder, List<string> listInfo, ref Dictionary<string, T> dictForLoaded) where T : UnityEngine.Object
    {
        if (listInfo != null)
        {
            if (dictForLoaded == null)
                dictForLoaded = new Dictionary<string, T>();
            int objectsCount = listInfo.Count;
            for (int i = 0; i < objectsCount; i++)
            {
                string resPath = parentFolder + listInfo[i];
                var objectLoaded = Resources.Load<T>(resPath);
                if (!objectLoaded)
                {
                    Debug.LogError("Couldn't load object of type " + typeof(T).ToString() + " from path \"" + resPath + "\".");
                    continue;
                }
                dictForLoaded.Add(objectLoaded.name, objectLoaded);
            }
        }
    }
    private void LoadResourcesAsync<T>(string parentFolder, List<string> listInfo, ref Dictionary<string, T> dictForLoaded) where T : UnityEngine.Object
    {
        if (listInfo != null)
        {
            if (dictForLoaded == null)
                dictForLoaded = new Dictionary<string, T>();
            int objectsCount = listInfo.Count;
            for (int i = 0; i < objectsCount; i++)
            {
                MonoListener.Instance.StartCoroutine(LoadResourceAsync(parentFolder + listInfo[i], dictForLoaded));
            }
        }
    }
    private IEnumerator LoadResourceAsync<T>(string path, Dictionary<string, T> dictForLoaded) where T : UnityEngine.Object
    {
        string blockerName = "blocker_" + path;
        Blocker blocker = new Blocker()
        {
            IsBlocking = true
        };
        IBlockable blockable = SceneManager.Instance as IBlockable;
        blockable.AddBlocker(blockerName, blocker);
        ResourceRequest resourceRequest = Resources.LoadAsync<T>(path);
        while (!resourceRequest.isDone)
        {
            yield return null;
        }
        Debug.Log("Loaded " + resourceRequest.asset.name);
        dictForLoaded.Add(resourceRequest.asset.name, (T)resourceRequest.asset);
        blocker.IsBlocking = false;
        blockable.RemoveBlocker(blockerName);
    }
}
#endregion

public interface IResourceManager
{
    event Action OnSceneResourcesUnloadedEvent;

    ResourcesHandler SceneResourcesHandler { get; }
    ResourcesHandler LocalizedSceneResourcesHandler { get; }
    ResourcesHandler PermanentResourcesHandler { get; }
    ResourcesHandler LocalizedPermanentResourcesHandler { get; }
    Dictionary<string, ResourcesHandler> ExternalResourcesHandlers { get; }

    void AddResourcesHandler(string name, ResourcesHandler resourcesHandler, ResourcesToLoad resourcesToLoad = null);
    ResourcesHandler GetResourcesHandler(string name);
    bool RemoveResourcesHandler(string name);
    void LoadToResourcesHandler(string name, ResourcesToLoad resourcesToLoad);
    void UnloadFromResourcesHandler(string name);
}

public class ResourceManager : ASingletoneService<ResourceManager, IResourceManager>, IResourceManager
{
    #region Events
    public event Action OnSceneResourcesUnloadedEvent;
    #endregion

    #region Properties
    public ResourcesHandler SceneResourcesHandler { get { return _sceneResourcesHandler; } }
    public ResourcesHandler LocalizedSceneResourcesHandler { get { return _localizedSceneResourcesHandler; } }
    public ResourcesHandler PermanentResourcesHandler { get { return _permanentResourcesHandler; } }
    public ResourcesHandler LocalizedPermanentResourcesHandler { get { return _localizedPermanentResourcesHandler; } }
    public Dictionary<string, ResourcesHandler> ExternalResourcesHandlers { get { return _externalResourcesHandlers; } }
    #endregion

    #region Public members
    public static readonly string RESOURCES_SYSTEM_PATH = "Assets/Resources/";
    public static readonly string LOCALIZED_PATH = "Localized/";
    public static readonly string AUDIO_CLIPS_PATH = "AudioClips/";
    public static readonly string OTHER_OBJECTS_PATH = "OtherObjects/";
    public static readonly string PREFABS_PATH = "Prefabs/";
    public static readonly string SPRITES_PATH = "Sprites/";
    public static readonly string TEXT_ASSETS_PATH = "TextAssets/";
    public static readonly string TEXTURES_PATH = "Textures/";
    #endregion

    #region Private members
    private ResourcesHandler _sceneResourcesHandler = new ResourcesHandler();
    private ResourcesHandler _localizedSceneResourcesHandler = new ResourcesHandler();
    private ResourcesHandler _permanentResourcesHandler = new ResourcesHandler();
    private ResourcesHandler _localizedPermanentResourcesHandler = new ResourcesHandler();
    private ResourcesToLoad _permanentResourcesToLoad = new ResourcesToLoad()
    {
        AudioClips = new List<string>
            {
                "btn_click_sound_2",
            },
    };
    private ResourcesToLoad _localizedPermanentResourcesToLoad = new ResourcesToLoad()
    {
        TextAssets = new List<string>
        {
            "PermanentPhrazes",
        },
        Sprites = new List<string>
        {
            "background",
        },
    };
    private Dictionary<string, ResourcesHandler> _externalResourcesHandlers = new Dictionary<string, ResourcesHandler>();
    private static readonly Dictionary<SceneEnum, ResourcesToLoad> _SCENES_RESOURCES_MAP = new Dictionary<SceneEnum, ResourcesToLoad>
    {
    };
    private static readonly Dictionary<SceneEnum, ResourcesToLoad> _SCENES_LOCALIZED_RESOURCES_MAP = new Dictionary<SceneEnum, ResourcesToLoad>
    {
    };
    #endregion

    #region Event handlers
    private void OnLocalizationChangedHandler(Localization localization)
    {
        _localizedPermanentResourcesHandler.Unload();
        _localizedSceneResourcesHandler.Unload();
        _localizedPermanentResourcesToLoad.ParentFolder = LOCALIZED_PATH + LocalizationManager.Instance.GetLocalization().ToString() + "/";
        _localizedPermanentResourcesHandler.Load(_localizedPermanentResourcesToLoad);
        var localizedSceneResourcesToLoad = _SCENES_LOCALIZED_RESOURCES_MAP.GetValueOrDefault(SceneManager.Instance.GetCurrentScene());
        if (localizedSceneResourcesToLoad != null)
        {
            localizedSceneResourcesToLoad.ParentFolder = LOCALIZED_PATH + LocalizationManager.Instance.GetLocalization().ToString() + "/";
            _localizedSceneResourcesHandler.Load(localizedSceneResourcesToLoad);
        }
    }
    private void OnLoadingStartedHandler(SceneEnum prevScene, SceneEnum nextScene)
    {
        _sceneResourcesHandler.Unload();
        _localizedSceneResourcesHandler.Unload();
        if (OnSceneResourcesUnloadedEvent != null)
            OnSceneResourcesUnloadedEvent();
        ResourcesToLoad resourcesToLoad = _SCENES_RESOURCES_MAP.GetValueOrDefault(nextScene);
        if (resourcesToLoad != null)
            _sceneResourcesHandler.LoadAsync(resourcesToLoad);
        var localizedSceneResourcesToLoad = _SCENES_LOCALIZED_RESOURCES_MAP.GetValueOrDefault(nextScene);
        if (localizedSceneResourcesToLoad != null)
        {
            localizedSceneResourcesToLoad.ParentFolder = LOCALIZED_PATH + LocalizationManager.Instance.GetLocalization().ToString() + "/";
            _localizedSceneResourcesHandler.Load(localizedSceneResourcesToLoad);
        }
    }
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        _permanentResourcesHandler.Load(_permanentResourcesToLoad);
    }
    public override void Subscribe()
    {
        LocalizationManager.Instance.OnLocalizationChangedEvent += OnLocalizationChangedHandler;
        SceneManager.Instance.OnLoadingStartedEvent += OnLoadingStartedHandler;

        var scene = SceneManager.Instance.GetCurrentScene();
        OnLoadingStartedHandler(scene, scene);
        OnLocalizationChangedHandler(LocalizationManager.Instance.GetLocalization());
    }
    public override void Load()
    {
    }
    #endregion

    #region Core
    public void AddResourcesHandler(string name, ResourcesHandler resourcesHandler, ResourcesToLoad resourcesToLoad = null)
    {
        _externalResourcesHandlers.Add(name, resourcesHandler);
        if (resourcesToLoad != null)
            resourcesHandler.Load(resourcesToLoad);
    }
    public ResourcesHandler GetResourcesHandler(string name)
    {
        return _externalResourcesHandlers[name];
    }
    public bool RemoveResourcesHandler(string name)
    {
        return _externalResourcesHandlers.Remove(name);
    }
    public void LoadToResourcesHandler(string name, ResourcesToLoad resourcesToLoad)
    {
        _externalResourcesHandlers[name].Load(resourcesToLoad);
    }
    public void UnloadFromResourcesHandler(string name)
    {
        _externalResourcesHandlers[name].Unload();
    }
    #endregion
    #endregion
}