﻿//#define WAIT_FOR_ADS_TO_LOAD

using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using Game.Extensions;

public enum CustomAdPosition
{
    TopLeft,
    Top,
    TopRight,

    Center,
    MIDDLE_CENTER,
    MIDDLE_RIGHT,

    BottomLeft,
    Bottom,
    BottomRight,
}

public interface IAdsProvider
{
    event Action OnBannerLoadedEvent;
    event Action OnBannerOpeningEvent;
    event Action OnBannerClosedEvent;

    event Action OnInterLoadedEvent;
    event Action OnInterOpeningEvent;
    event Action OnInterClosedEvent;

    event Action OnRewardedLoadedEvent;
    event Action OnRewardedOpeningEvent;
    event Action OnRewardedStartedEvent;
    event Action<Reward> OnRewardedRewardedEvent;
    event Action OnRewardedClosedEvent;

    void LoadBanner();
    void ShowBanner();
    void HideBanner();
    void SetBannerPosition(CustomAdPosition customAdPosition);
    float GetBannerWidth();
    float GetBannerHeight();

    void LoadInter();
    bool IsInterLoaded();
    void ShowInter();

    void LoadRewarded();
    bool IsRewardedLoaded();
    void ShowRewarded();
}

public interface IAdsController
{
}

public class AdsController : ASingletoneService<AdsController, IAdsController>, IAdsController
{
    #region Private members
    private IAdsProvider _adsProvider = null;
    private IBlockable _sceneManagerAsBlockable = null;
#if WAIT_FOR_ADS_TO_LOAD
    private Blocker _bannerNotLoadedBlocker = null;
    private Blocker _interNotLoadedBlocker = null;
#endif
    private Blocker _interShowingBlocker = null;
    private static readonly string _BANNER_NOT_LOADED_BLOCKER_NAME = "BANNER_NOT_LOADED";
    private static readonly string _INTER_NOT_LOADED_BLOCKER_NAME = "INTER_NOT_LOADED";
    private static readonly string _INTER_SHOWING_BLOCKER_NAME = "INTER_SHOWING";
    private static readonly float _INTER_DELAY = 45.0f;
    private float _lastUnscaledTimeInterHidden = 0.0f;

    private static readonly Dictionary<SceneEnum, bool> _BANNER_RESTRICTIONS_MAP = new Dictionary<SceneEnum, bool>
    {
        { SceneEnum.EntryPoint, false },
        { SceneEnum.MainMenu, false },
        { SceneEnum.Settings, false },
        { SceneEnum.AboutUs, false },
    };
    private static bool _DEFAULT_BANNER_ALLOWED = true;

    [Flags]
    private enum InterAvailableFlag : byte
    {
        NotAllowed = 0,
        Before = 1,
        After = 2,
        BeforeAndAfter = Before | After,
    }

    private static readonly Dictionary<SceneEnum, InterAvailableFlag> _INTER_RESTRICTIONS_MAP = new Dictionary<SceneEnum, InterAvailableFlag>
    {
        { SceneEnum.EntryPoint, InterAvailableFlag.NotAllowed },
        { SceneEnum.MainMenu, InterAvailableFlag.Before },
        { SceneEnum.Settings, InterAvailableFlag.Before },
        { SceneEnum.AboutUs, InterAvailableFlag.NotAllowed },
    };
    private static InterAvailableFlag _INTER_DEFAULT_FLAG = InterAvailableFlag.BeforeAndAfter;

    private static readonly Dictionary<SceneEnum, CustomAdPosition> _BANNER_OVERRIDED_POSITIONS = new Dictionary<SceneEnum, CustomAdPosition>
    {
        { SceneEnum.Map, CustomAdPosition.Top },
    };
    private static readonly CustomAdPosition _DEFAULT_BANNER_POSITION = CustomAdPosition.Bottom;
    #endregion

    #region Event handlers
    private void OnLoadingStartedHandler(SceneEnum prevScene, SceneEnum nextScene)
    {
        _adsProvider.HideBanner();
        InterAvailableFlag prevSceneRestriction = _INTER_RESTRICTIONS_MAP.GetValueOrDefault(prevScene, _INTER_DEFAULT_FLAG);
        InterAvailableFlag nextSceneRestriction = _INTER_RESTRICTIONS_MAP.GetValueOrDefault(nextScene, _INTER_DEFAULT_FLAG);
        bool canShowInter = ((byte)prevSceneRestriction).HasFlag((byte)InterAvailableFlag.After) && ((byte)nextSceneRestriction).HasFlag((byte)InterAvailableFlag.Before);
        if (canShowInter)
        {
            if (Time.unscaledTime - _lastUnscaledTimeInterHidden >= _INTER_DELAY) {
                if (_adsProvider.IsInterLoaded())
                {
                    _adsProvider.ShowInter();
                }
            }
        }
    }
    private void OnPreSceneLoadedHandler(SceneEnum scene)
    {
        bool bannerAllowed = _BANNER_RESTRICTIONS_MAP.GetValueOrDefault(scene, _DEFAULT_BANNER_ALLOWED);
        if (bannerAllowed)
        {
            _adsProvider.ShowBanner();
        }
    }

    #region Ads provider events handling
    private void OnBannerLoadedHandler()
    {
#if WAIT_FOR_ADS_TO_LOAD
        _bannerNotLoadedBlocker.IsBlocking = false;
#endif
    }
    private void OnInterLoadedHandler()
    {
#if WAIT_FOR_ADS_TO_LOAD
        _interNotLoadedBlocker.IsBlocking = false;
#endif
    }
    private void OnInterClosedHandler()
    {
        _lastUnscaledTimeInterHidden = Time.unscaledDeltaTime;
        _interShowingBlocker.IsBlocking = false;
        _adsProvider.LoadInter();
    }
    private void OnRewardedLoadedHandler()
    {
    }
    private void OnRewardedRewardedHandler(Reward reward)
    {
        Debug.Log("We got a reward! Type: \"" + reward.Type + "\" amount: \"" + reward.Amount + "\".");
    }
    #endregion
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        _adsProvider = new GoogleAdsProvider();
        _adsProvider.HideBanner();
        _adsProvider.LoadBanner();
        _adsProvider.LoadInter();
        _adsProvider.LoadRewarded();

#if WAIT_FOR_ADS_TO_LOAD
        _bannerNotLoadedBlocker = new Blocker();
        _interNotLoadedBlocker = new Blocker();
#endif
        _interShowingBlocker = new Blocker();

    }
    public override void Subscribe()
    {
        SceneManager.Instance.OnLoadingStartedEvent += OnLoadingStartedHandler;
        SceneManager.Instance.OnPreSceneLoadedEvent += OnPreSceneLoadedHandler;
    }
    public override void Load()
    {
#if WAIT_FOR_ADS_TO_LOAD
        _bannerNotLoadedBlocker.IsBlocking = true;
        _interNotLoadedBlocker.IsBlocking = true;
#endif

        _sceneManagerAsBlockable = SceneManager.Instance as IBlockable;
#if WAIT_FOR_ADS_TO_LOAD
        _sceneManagerAsBlockable.AddBlocker(_BANNER_NOT_LOADED_BLOCKER_NAME, _bannerNotLoadedBlocker);
        _sceneManagerAsBlockable.AddBlocker(_INTER_NOT_LOADED_BLOCKER_NAME, _interNotLoadedBlocker);
#endif
        _sceneManagerAsBlockable.AddBlocker(_INTER_SHOWING_BLOCKER_NAME, _interShowingBlocker);

        _adsProvider.OnBannerLoadedEvent += OnBannerLoadedHandler;
        _adsProvider.OnInterLoadedEvent += OnInterLoadedHandler;
        _adsProvider.OnInterClosedEvent += OnInterClosedHandler;
        _adsProvider.OnRewardedLoadedEvent += OnRewardedLoadedHandler;
        _adsProvider.OnRewardedRewardedEvent += OnRewardedRewardedHandler;
    }
    #endregion
    #endregion

    #region Private methods
    //private void PrivateMethod()
    //{
    //}
    #endregion
}
