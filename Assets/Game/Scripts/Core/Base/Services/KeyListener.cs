﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Game.Extensions;

public enum KeyActionType
{
    DOWN,
    PRESS,
    UP,
}

public interface IKeyListener
{
    void AddKeyAction(KeyActionType keyActionType, KeyCode keyCode, Action action);
    bool RemoveKeyAction(KeyActionType keyActionType, KeyCode keyCode, Action action);
    void ClearKeyActions(KeyActionType keyActionType, KeyCode keyCode);
    void ClearAllActions(KeyActionType keyActionType);
}

public class KeyListener : ASingletoneService<KeyListener, IKeyListener>, IKeyListener
{
    #region Private members
    private Dictionary<KeyActionType, Dictionary<KeyCode, List<Action>>> _keyActionsDict = new Dictionary<KeyActionType, Dictionary<KeyCode, List<Action>>>
    {
        { KeyActionType.DOWN, new Dictionary<KeyCode, List<Action>>() },
        { KeyActionType.PRESS, new Dictionary<KeyCode, List<Action>>() },
        { KeyActionType.UP, new Dictionary<KeyCode, List<Action>>() },
    };
    #endregion

    #region Event handlers
    private void OnUpdateHandler()
    {
        foreach (var kvpActionTypes in _keyActionsDict)
            switch (kvpActionTypes.Key)
            {
                case KeyActionType.DOWN:
                    foreach (var kvpKeyAction in kvpActionTypes.Value)
                        if (Input.GetKeyDown(kvpKeyAction.Key))
                        {
                            int length = kvpKeyAction.Value.Count;
                            for (int i = 0; i < length; i++)
                                kvpKeyAction.Value[i]();
                        }
                    break;
                case KeyActionType.PRESS:
                    foreach (var kvpKeyAction in kvpActionTypes.Value)
                        if (Input.GetKey(kvpKeyAction.Key))
                        {
                            int length = kvpKeyAction.Value.Count;
                            for (int i = 0; i < length; i++)
                                kvpKeyAction.Value[i]();
                        }
                    break;
                case KeyActionType.UP:
                    foreach (var kvpKeyAction in kvpActionTypes.Value)
                        if (Input.GetKeyUp(kvpKeyAction.Key))
                        {
                            int length = kvpKeyAction.Value.Count;
                            for (int i = 0; i < length; i++)
                                kvpKeyAction.Value[i]();
                        }
                    break;
                default:
                    break;
            }
    }
    #endregion

    #region Public methods
    #region Service
    public override void Subscribe()
    {
        MonoListener.Instance.OnUpdateEvent += OnUpdateHandler;
    }
    #endregion

    #region Core
    public void AddKeyAction(KeyActionType keyActionType, KeyCode keyCode, Action action)
    {
        _keyActionsDict[keyActionType].GetValueOrCreate(keyCode).Add(action);
    }
    public bool RemoveKeyAction(KeyActionType keyActionType, KeyCode keyCode, Action action)
    {
        return _keyActionsDict[keyActionType].GetValueOrCreate(keyCode).Remove(action);
    }
    public void ClearKeyActions(KeyActionType keyActionType, KeyCode keyCode)
    {
        _keyActionsDict[keyActionType].GetValueOrCreate(keyCode).Clear();
    }
    public void ClearAllActions(KeyActionType keyActionType)
    {
        _keyActionsDict[keyActionType].Clear();
    }
    #endregion
    #endregion
}
