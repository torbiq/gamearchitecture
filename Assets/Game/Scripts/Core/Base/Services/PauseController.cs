﻿using System;
using UnityEngine;

public interface IPauseController
{
    float LastSavedTimeScale { get; }
    bool IsPaused { get; }

    event Action OnPauseEvent;
    event Action OnUnpauseEvent;

    bool SetPause(bool value);
    void Switch();
    void Pause();
    void Unpause();
}

public class PauseController : ASingletoneService<PauseController, IPauseController>, IPauseController
{
    #region Properties
    public float LastSavedTimeScale { get; private set; }
    public bool IsPaused { get; private set; }
    #endregion

    #region Events
    public event Action OnPauseEvent;
    public event Action OnUnpauseEvent;
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        LastSavedTimeScale = Time.timeScale;
        IsPaused = Time.timeScale == 0.0f;
    }
    #endregion

    #region Core
    public bool SetPause(bool value)
    {
        if (value && !IsPaused)
            Pause();
        else if (!value && IsPaused)
            Unpause();
        else
            return false;
        return true;
    }
    public void Switch()
    {
        if (IsPaused)
            Unpause();
        else
            Pause();
    }
    public void Pause()
    {
        LastSavedTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        if (OnPauseEvent != null)
            OnPauseEvent();
    }
    public void Unpause()
    {
        Time.timeScale = LastSavedTimeScale;
        if (OnUnpauseEvent != null)
            OnUnpauseEvent();
    }
    #endregion
    #endregion
}
