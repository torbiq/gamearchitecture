﻿#define ADS_DEBUG

using GoogleMobileAds.Api;
using System;
using UnityEngine;

public class GoogleAdsProvider : IAdsProvider
{
    #region Events
    public event Action OnBannerLoadedEvent;
    public event Action OnBannerOpeningEvent;
    public event Action OnBannerClosedEvent;

    public event Action OnInterLoadedEvent;
    public event Action OnInterOpeningEvent;
    public event Action OnInterClosedEvent;

    public event Action OnRewardedLoadedEvent;
    public event Action OnRewardedOpeningEvent;
    public event Action OnRewardedStartedEvent;
    public event Action<Reward> OnRewardedRewardedEvent;
    public event Action OnRewardedClosedEvent;
    #endregion

    public GoogleAdsProvider()
    {
#if ADS_DEBUG
        _APP_ID = "ca-app-pub-3940256099942544~3347511713";
        _REWARDED_ID = "ca-app-pub-3940256099942544/5224354917";
        _INTERSTITIAL_ID = "ca-app-pub-3940256099942544/1033173712";
        _BANNER_ID = "ca-app-pub-3940256099942544/6300978111";
#elif UNITY_ANDROID
        _APP_ID = "ca-app-pub-3940256099942544~3347511713";
        _REWARDED_ID = "ca-app-pub-3940256099942544/5224354917";
        _INTERSTITIAL_ID = "ca-app-pub-3940256099942544/1033173712";
        _BANNER_ID = "ca-app-pub-3940256099942544/6300978111";
#elif UNITY_IOS
        _APP_ID = "ca-app-pub-3940256099942544~3347511713";
        _REWARDED_ID = "ca-app-pub-3940256099942544/5224354917";
        _INTERSTITIAL_ID = "ca-app-pub-3940256099942544/1033173712";
        _BANNER_ID = "ca-app-pub-3940256099942544/6300978111";
#endif
        MobileAds.Initialize(_APP_ID);
        _interAd = new InterstitialAd(_INTERSTITIAL_ID);
        _bannerAd = new BannerView(_BANNER_ID, AdSize.SmartBanner, AdPosition.Bottom);
        _rewardedAd = RewardBasedVideoAd.Instance;

        _interAd.OnAdLoaded += OnInterLoadedHandler;
        _interAd.OnAdFailedToLoad += OnInterFailedToLoadHandler;
        _interAd.OnAdOpening += OnInterOpeningHandler;
        _interAd.OnAdClosed += OnInterClosedHandler;

        _bannerAd.OnAdLoaded += OnBannerLoadedHandler;
        _bannerAd.OnAdFailedToLoad += OnBannerFailedToLoadHandler;
        _bannerAd.OnAdOpening += OnBannerOpeningHandler;
        _bannerAd.OnAdClosed += OnBannerClosedHandler;

        _rewardedAd.OnAdLoaded += OnRewardedLoadedHandler;
        _rewardedAd.OnAdFailedToLoad += OnRewardedFailedToLoadHandler;
        _rewardedAd.OnAdOpening += OnRewardedOpeningHandler;
        _rewardedAd.OnAdStarted += OnRewardedStartedHandler;
        _rewardedAd.OnAdClosed += OnRewardedClosedHandler;
        _rewardedAd.OnAdRewarded += OnRewardedRewardedHandler;
    }

    #region Private members
    private readonly string _APP_ID;
    private readonly string _REWARDED_ID;
    private readonly string _INTERSTITIAL_ID;
    private readonly string _BANNER_ID;
    private InterstitialAd _interAd;
    private BannerView _bannerAd;
    private RewardBasedVideoAd _rewardedAd;
    #endregion

    #region Core
    public void LoadBanner()
    {
        _bannerAd.LoadAd(new AdRequest.Builder().Build());
    }
    public void ShowBanner()
    {
        _bannerAd.Show();
    }
    public void HideBanner()
    {
        _bannerAd.Hide();
    }
    public void SetBannerPosition(CustomAdPosition customAdPosition)
    {
        switch (customAdPosition)
        {
            case CustomAdPosition.TopLeft:
                _bannerAd.SetPosition(AdPosition.TopLeft);
                break;
            case CustomAdPosition.Top:
                _bannerAd.SetPosition(AdPosition.Top);
                break;
            case CustomAdPosition.TopRight:
                _bannerAd.SetPosition(AdPosition.TopRight);
                break;
            case CustomAdPosition.Center:
                _bannerAd.SetPosition(AdPosition.Center);
                break;
            case CustomAdPosition.BottomLeft:
                _bannerAd.SetPosition(AdPosition.BottomLeft);
                break;
            case CustomAdPosition.Bottom:
                _bannerAd.SetPosition(AdPosition.Bottom);
                break;
            case CustomAdPosition.BottomRight:
                _bannerAd.SetPosition(AdPosition.BottomRight);
                break;
            default:
                Debug.LogError("CustomAdPosition " + customAdPosition.ToString() + " not recognized.");
                break;
        }
    }
    public float GetBannerWidth()
    {
        return _bannerAd.GetWidthInPixels();
    }
    public float GetBannerHeight()
    {
        return _bannerAd.GetHeightInPixels();
    }

    public void LoadInter()
    {
        _interAd.LoadAd(new AdRequest.Builder().Build());
    }
    public bool IsInterLoaded()
    {
        return _interAd.IsLoaded();
    }
    public void ShowInter()
    {
        _interAd.Show();
    }

    public void LoadRewarded()
    {
        _rewardedAd.LoadAd(new AdRequest.Builder().Build(), _REWARDED_ID);
    }
    public bool IsRewardedLoaded()
    {
        return _rewardedAd.IsLoaded();
    }
    public void ShowRewarded()
    {
        _rewardedAd.Show();
    }
    #endregion

    #region Event handlers
    private void OnBannerLoadedHandler(object sender, EventArgs e)
    {
        if (OnBannerLoadedEvent != null)
            OnBannerLoadedEvent();
    }
    private void OnBannerFailedToLoadHandler(object sender, AdFailedToLoadEventArgs e)
    {
        Debug.LogError("Banner failed to load. Details: " + e.Message);
    }
    private void OnBannerOpeningHandler(object sender, EventArgs e)
    {
        if (OnBannerOpeningEvent != null)
            OnBannerOpeningEvent();
    }
    private void OnBannerClosedHandler(object sender, EventArgs e)
    {
        if (OnBannerClosedEvent != null)
            OnBannerClosedEvent();
    }

    private void OnInterLoadedHandler(object sender, EventArgs e)
    {
        if (OnInterLoadedEvent != null)
            OnInterLoadedEvent();
    }
    private void OnInterFailedToLoadHandler(object sender, AdFailedToLoadEventArgs e)
    {
        Debug.LogError("Banner failed to load. Details: " + e.Message);
    }
    private void OnInterOpeningHandler(object sender, EventArgs e)
    {
        if (OnInterOpeningEvent != null)
            OnInterOpeningEvent();
    }
    private void OnInterClosedHandler(object sender, EventArgs e)
    {
        if (OnInterClosedEvent != null)
            OnInterClosedEvent();
    }

    private void OnRewardedLoadedHandler(object sender, EventArgs e)
    {
        if (OnRewardedLoadedEvent != null)
            OnRewardedLoadedEvent();
    }
    private void OnRewardedFailedToLoadHandler(object sender, AdFailedToLoadEventArgs e)
    {
        Debug.LogError("Rewarded failed to load. Details: " + e.Message);
    }
    private void OnRewardedOpeningHandler(object sender, EventArgs e)
    {
        if (OnRewardedOpeningEvent != null)
            OnRewardedOpeningEvent();
    }
    private void OnRewardedStartedHandler(object sender, EventArgs e)
    {
        if (OnRewardedStartedEvent != null)
            OnRewardedStartedEvent();
    }
    private void OnRewardedRewardedHandler(object sender, Reward e)
    {
        if (OnRewardedRewardedEvent != null)
            OnRewardedRewardedEvent(e);
    }
    private void OnRewardedClosedHandler(object sender, EventArgs e)
    {
        if (OnRewardedClosedEvent != null)
            OnRewardedClosedEvent();
    }
    #endregion
}
