﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Game.Extensions;

public interface ISceneLoadingUIController {

}

public class SceneLoadingUIController : ASingletoneService<SceneLoadingUIController, ISceneLoadingUIController>, ISceneLoadingUIController
{
    #region Private members
    #region Serialized
    [SerializeField] private Image _fadeImage;
    [SerializeField] private Image _progressBarBg;
    [SerializeField] private Image _progressBarImage;
    #endregion
    #endregion

    #region Public methods
    #region Service
    public override void Init()
    {
        _fadeImage.color = _fadeImage.color.GetWithAlpha(0.0f);
        _progressBarImage.fillAmount = 0.0f;
        _progressBarBg.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
    public override void Subscribe()
    {
        SceneManager.Instance.OnWaitingBeforeLoadEvent += OnWaitingBeforeLoadHandler;
        SceneManager.Instance.OnProgressUpdateEvent += OnProgressUpdateHandler;
        SceneManager.Instance.OnWaitingBeforeShowEvent += OnWaitingBeforeShowHandler;
    }
    #endregion
    #endregion

    #region Event handlers
    private void OnWaitingBeforeLoadHandler(float delay)
    {
        gameObject.SetActive(true);
        _progressBarImage.fillAmount = 0.0f;
        _fadeImage.DOKill(false);
        _fadeImage.DOFade(1.0f, delay).OnComplete(()=> {
            _progressBarBg.gameObject.SetActive(true);
        });
    }
    private void OnProgressUpdateHandler(float progress)
    {
        _progressBarImage.fillAmount = progress;
    }
    private void OnWaitingBeforeShowHandler(float delay)
    {
        _progressBarBg.gameObject.SetActive(false);
        _fadeImage.DOKill(false);
        _fadeImage.DOFade(0.0f, delay).OnComplete(()=> {
            gameObject.SetActive(false);
        });
    }
    #endregion
}
