﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Extensions {
    public static class EList {
        /// <summary>
        /// Casts Array<T> to List<T>.
        /// </summary>
        public static List<T> ToList<T>(this T[] array) {
            List<T> returnedlist = new List<T>();
            for (int i = 0; i < array.Length; i++) {
                returnedlist.Add(array[i]);
            }
            return returnedlist;
        }
        /// <summary>
        /// Returns a list with indexes range from 0 to count - 1.
        /// </summary>
        public static List<int> ToListOfIndexes(this int count) {
            List<int> returned = new List<int>();
            while (count > 0) {
                count--;
                returned.Add(count);
            }
            return returned;
        }
        /// <summary>
        /// Returns random element and removes it from list (throwes exception if it's empty).
        /// </summary>
        /// <typeparam name="T">List type.</typeparam>
        /// <param name="list">This list.</param>
        /// <returns>Random element.</returns>
        public static T EraseRandomGot<T>(this List<T> list) {
            // Throw exceptions if list is empty.
            if (list.Count == 0) {
                throw new System.Exception("List is empty.");
            }
            int indexTaken = Random.Range(0, list.Count);
            T taken = list[indexTaken];
            list.RemoveAt(indexTaken);
            return taken;
        }
        /// <summary>
        /// Shuffles all elements in this list.
        /// </summary>
        public static void Shuffle<T>(this List<T> list) {
            // Create temporary list for elements to contain them.
            List<T> tmpContainer = new List<T>();
            tmpContainer.AddRange(list);
            list.Clear();
            while (tmpContainer.Count != 0) {
                // Add random element from container on the top.
                list.Add(tmpContainer.EraseRandomGot());
            }
        }
        /// <summary>
        /// Cyclically returns an index represented by origin position and needed delta to count from.
        /// </summary>
        /// <param name="indexOrigin">Index to start count from it.</param>
        /// <param name="deltaIndexes">Delta in indexes amount.</param>
        /// <returns></returns>
        public static int GetCyclicIndexByDelta<T>(this List<T> list, int indexOrigin, int deltaIndexes) {
            int indexCounted = indexOrigin + (deltaIndexes % list.Count);
            return (indexCounted < 0 ? indexCounted + list.Count : indexCounted) % list.Count;
        }
        /// <summary>
        /// Returns last element from list and removes it.
        /// </summary>
        public static T GetLastAndRemove<T>(this List<T> list) {
            if (list.Count == 0) {
                return default(T);
            }
            int indexReturned = list.Count - 1;
            var itemReturned = list[indexReturned];
            list.RemoveAt(indexReturned);
            return itemReturned;
        }
    }
}
