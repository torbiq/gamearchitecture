﻿using UnityEngine;
using DG.Tweening;
using System;

namespace Game.Extensions
{
    public static class Extensions
    {
        public static int ToInt(this bool val)
        {
            if (val)
                return 1;
            return 0;
        }
        public static bool ToBool(this int val)
        {
            return val != 0;
        }

        public static bool HasFlag(this byte val, byte valToCheck)
        {
            return (val & valToCheck) == valToCheck;
        }
        public static bool HasFlag(this Int16 val, Int16 valToCheck)
        {
            return (val & valToCheck) == valToCheck;
        }
        public static bool HasFlag(this Int32 val, Int32 valToCheck)
        {
            return (val & valToCheck) == valToCheck;
        }
        public static bool HasFlag(this Int64 val, Int64 valToCheck)
        {
            return (val & valToCheck) == valToCheck;
        }

        public static T ParseStringToEnum<T>(string value) where T : struct
        {
            return (T)System.Enum.Parse(typeof(T), value);
        }
        public static int GetEnumSize<EnumType>()
        {
            return System.Enum.GetNames(typeof(EnumType)).Length;
        }
        public static int GetEnumSize(this System.Enum enumValue)
        {
            return System.Enum.GetNames(enumValue.GetType()).Length;
        }
        public static void KillIfPlaying(this Tween tween, bool complete = false)
        {
            if (tween != null)
                if (tween.IsPlaying())
                    tween.Kill(complete);
        }
        public static float GetClipLength(this Animator animator, string clipName)
        {
            int length = animator.runtimeAnimatorController.animationClips.Length;
            for (int i = 0; i < length; ++i)
            {
                var clip = animator.runtimeAnimatorController.animationClips[i];
                if (clip.name == clipName)
                    return clip.length;
            }
            return 0f;
        }
    }
}