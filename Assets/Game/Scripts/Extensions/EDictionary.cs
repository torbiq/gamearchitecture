﻿using System.Collections.Generic;

namespace Game.Extensions {
    public static class EDictionary {
        public static TValue GetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue defaultValue = default(TValue)) {
            TValue outValue;
            if (dict.TryGetValue(key, out outValue)) {
                return outValue;
            }
            return defaultValue;
        }
        public static TValue GetValueOrCreate<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key) where TValue : class, new()
        {
            TValue outValue;
            if (dict.TryGetValue(key, out outValue))
            {
                return outValue;
            }
            TValue createdValue = new TValue();
            dict.Add(key, createdValue);
            return createdValue;
        }
    }
}
