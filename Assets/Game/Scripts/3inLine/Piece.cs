﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
#if UNITY_EDITOR
    [RequireComponent(typeof(SpriteRenderer), typeof(CircleCollider2D))]
#endif
    public class Piece : MonoBehaviour
    {
        public event Action<Player> OnPlayerChangedEvent;

        private Player _player = null;
        public Player Player
        {
            get { return _player; }
            set
            {
                _player = value;
                if (OnPlayerChangedEvent != null)
                    OnPlayerChangedEvent(_player);
            }
        }

        private void OnPlayerChangedHandler(Player player)
        {

        }
        
        private void OnEnable()
        {
            
        }
        private void OnDisable()
        {
            
        }
    }
}