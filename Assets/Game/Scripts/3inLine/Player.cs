﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Player : MonoBehaviour
    {
        public event Action<Color> OnColorChangedEvent;
        public event Action<string> OnNicknameChangedEvent;

        private Color _color;
        public Color Color
        {
            get { return _color; }
            set
            {
                _color = value;
                if (OnColorChangedEvent != null)
                    OnColorChangedEvent(_color);
            }
        }

        private string _nickname;
        public string Nickname
        {
            get { return _nickname; }
            set
            {
                _nickname = value;
                if (OnNicknameChangedEvent != null)
                    OnNicknameChangedEvent(_nickname);
            }
        }
    }
}