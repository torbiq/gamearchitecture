﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Board : MonoBehaviour
    {
        private Piece[] _piecesContained = null;

        public bool TryPlacePiece(int index, Piece piece)
        {
            if (_piecesContained[index] != null)
            {
                Debug.LogError("");
                return false;
            }
            _piecesContained[index] = piece;
            return true;
        }

        private void Awake()
        {
            
        }
    }
}
