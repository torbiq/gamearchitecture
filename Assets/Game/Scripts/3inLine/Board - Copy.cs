﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Slot : MonoBehaviour
    {
        public event Action<Piece> OnPieceChangedEvent;

        private Piece _piece = null;
        public Piece Piece
        {
            get { return _piece; }
            set
            {
                _piece = value;
                if (OnPieceChangedEvent != null)
                    OnPieceChangedEvent(_piece);
            }
        }
    }
}
